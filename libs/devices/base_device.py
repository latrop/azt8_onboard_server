import json
import threading
import time


class BaseDevice(object):
    """
    A base class for all devices, fake and real
    """
    def __init__(self, out_queue, logger):
        self.out_queue = out_queue
        self.logger = logger
        self.name = "Base"
        self.wait_time = 0.5
        # The device info dict contain all the info that can be retrieved from the
        # device by the server and sent to the client
        self.device_info = {"Connected": False}
        # The connected attribute reflects actual state of the device
        self.connected = False
        # If keep connected is true, the device will try to re-establish
        # connection if it is lost for some reason
        self.keep_connected = False
        self.is_alive = True  # Set to false to destroy the object

    def make_msg(self, **kwargs):
        """
        Method creates a message in a format that is acceptable to
        the client: a tuple with a device name and a dictionary of parameters
        """
        data = (self.name, kwargs)
        msg = json.dumps(data) + "\n"
        return msg

    def run_command(self, command_name, params=None):
        if params is None:
            params = []
        try:
            command = getattr(self, command_name)
        except AttributeError:
            # No command with such name
            self.logger.log(f"[Error] {self.name} does not have '{command_name}' command")
            return
        command(*params)

    def ping(self, *args):
        msg = self.make_msg(response="Pong")
        self.out_queue.put(msg)

    def connect(self):
        self.logger.log(f"Device {self.name}: connecting...")
        self.connected = True
        self.keep_connected = True
        threading.Thread(target=self.watchdog).start()
        self.logger.log(f"Device {self.name}: connected")
        return True

    def disconnect(self, keep_connected=False):
        """
        Just disconnect from the physical device, but do not destroy the object yet.
        """
        if self.connected:
            self.logger.log(f"Device {self.name}: disconnecting...")
            self.connected = False
            self.keep_connected = False
            self.logger.log(f"Device {self.name}: disconnected")

    def delete(self):
        """
        Shut everything down completely, no way back. Only the creation of a new instance
        will allow to connect to the physical device
        """
        self.is_alive = False
        if self.connected:
            self.disconnect()

    def watchdog(self):
        """
        This method runs in a background and checks the connection via check_device
        method periodically. If the connection is lost, it tries to reestablich it
        """
        while self.keep_connected:
            self.device_info_updater()
            if self.device_info["Connected"] is False:
                # No connection! Reconnect
                self.logger.log(f"[Error] {self.name}: connection lost")
                print("WD device info:        ", self.device_info)
                self.reconnect()
            time.sleep(self.wait_time)

    def reconnect(self):
        """
        Fix connection to the device
        """
        self.device_info["Connected"] = True

    def device_info_updater(self):
        """
        This method updates all the info that
        can be provided to the server by the device
        """
        while self.is_alive is True:
            if self.connected:
                self.device_info = {"Connected": True}
            else:
                self.device_info = {"Connected": False}
            time.sleep(0.1)

    def get_state(self):
        return self.device_info
