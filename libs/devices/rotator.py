import time
import queue
from serial import Serial
from serial import SerialException
import threading
from .base_device import BaseDevice


class Rotator(BaseDevice):
    def __init__(self, out_queue, logger, address):
        super(Rotator, self).__init__(out_queue, logger)
        self.address = address
        self.name = "Rotator"
        self.device = None
        self.posang = None
        self.temperature = None
        self.moving = False
        self.out_queue = queue.Queue()  # For sending messages to the device
        self.in_buffer = ""  # For storing income messages
        self.timeout = 5  # Sec
        self.last_message_timestamp = 1e10
        self.wait_time = 1.25
        self.connected = False

    def connect(self):
        self.logger.log(f"Connecting to rotator at {self.address}...")
        try:
            self.device = Serial(self.address, baudrate=19200, timeout=0.25)
        except SerialException:
            return False
        # Set the rotator to the manual mode
        for i in range(5):
            self.device.write("CCLINK".encode())
            time.sleep(1)
            try:
                response = self.device.read(2).decode()
            except SerialException:
                return False
            if "!" in response:
                break
        else:
            self.logger.log("Can't set rotator to manual mode")
        self.logger.log(f"Connected to rotator at {self.address}")
        self.connected = True
        self.device_info["Connected"] = True
        self.keep_connected = True
        threading.Thread(target=self.communicate).start()
        threading.Thread(target=self.watchdog).start()

    def disconnect(self):
        if not self.connected:
            return
        self.logger.log(f"Disconnecting from rotator at {self.address}...")
        self.device.close()
        self.connected = False
        self.device_info["Connected"] = False
        self.keep_connected = False
        # Wait for communication to shut down
        while 1:
            if self.communicating is False:
                break
            time.sleep(0.1)
        self.logger.log(f"Disconnected from rotator at {self.address}...")

    def reconnect(self):
        self.logger.log(f"Trying to reestablish connection to serial {self.address}")
        try:
            self.device.close()
        except SerialException:
            pass
        try:
            self.device = Serial(self.address, baudrate=19200, timeout=0.25)
        except (SerialException, OSError):
            return False
        self.logger.log(f"Connection to rotator at {self.address} reestablished")

    def communicate(self):
        """
        Sends the messages from the out_queue to the device and
        recieves messages from the device and stores them in the
        in_queue. Runs in the background
        """
        self.communicating = True
        new_data = ""
        try:
            while self.connected:
                # Send the message
                try:
                    message = self.out_queue.get_nowait()
                except queue.Empty:
                    # No new messagen in the queue
                    message = None
                if message is not None:
                    try:
                        self.device.write(message.encode())
                    except SerialException:
                        # Connection is probably lost. But we do not handle it here. Let's watchdog
                        # detect the absence of the connection and does reconnection
                        pass
                # Read everything from the buffer
                try:
                    new_data = self.device.read(32).decode()
                    # print(new_data)
                except (SerialException, TypeError):
                    # Connection is probably lost. But we do not handle it here. Let's watchdog
                    # detect the absence of the connection and does reconnection
                    pass
                if new_data != "":
                    self.in_buffer += new_data
                    # print("buffer:", self.in_buffer)
                    self.parse_device_output()
                time.sleep(0.1)
        finally:
            self.communicating = False

    def parse_device_output(self):
        while 1:
            # Parse data until there is something to parse
            if self.in_buffer == "":
                # Nothing to parse.
                return
            lines = self.in_buffer.split("\n")
            if len(lines) == 0:
                # Nothing to parse
                return
            message = lines[0].strip()
            print("message", message)
            self.last_message_timestamp = time.time()  # Let's note when was the last message recieved
            self.in_buffer = "\n".join(lines[1:])
            if "F" in message.strip():
                # Moving is finished
                self.moving = False
                continue
            if message.strip().startswith("!"):
                # Rotator in moving
                self.moving = True
                continue
            if message.strip().isnumeric():
                # New value of the position angle returned
                new_value = int(message)
                if new_value < 180:
                    self.posang = new_value
                else:
                    self.posang = new_value - 360
                continue

    def move_abs_posang(self, posang):
        """
        TODO: check what is zero point. Is it 180?
        """
        try:
            posang = int(posang)
        except ValueError:
            return
        if 0 <= posang <= 180:
            self.out_queue.put(f"CPA{posang:03}")
        elif -180 <= posang < 0:
            self.out_queue.put(f"CPA{posang+360:03}")

    def device_info_updater(self):
        # The method has to obtain new values of the device parrameters and
        # also check if the connection is lost. Since the communication with
        # the device goes in an acyncroneous way, we cant rely on the get_position
        #  function returning NaNs. Instead we check how
        # much time have passed since the last message. If it is greater than
        # the timeout, the connection is probably lost
        # TODO: add list_ports.compots() here
        self.out_queue.put("CGETPA")  # request new temerature value
        if (time.time() - self.last_message_timestamp > self.timeout) or (self.connected is False):
            self.device_info = {"Connected": False, "Posang": None,
                                "Moving": None}
            return
        self.device_info = {"Connected": self.connected,
                            "Posang": self.posang,
                            "Moving": self.moving}
