import time
from serial import Serial
from serial import SerialException
import serial.tools.list_ports as list_ports
import threading
import queue
from .base_device import BaseDevice


class Focuser(BaseDevice):
    def __init__(self, out_queue, logger, address):
        super(Focuser, self).__init__(out_queue, logger)
        self.address = address
        self.name = "Focuser"
        self.device = None
        self.position = None
        self.temperature = None
        self.moving = False
        self.out_queue = queue.Queue()  # For sending messages to the device
        self.in_buffer = ""  # For storing income messages
        self.timeout = 5  # Sec
        self.wait_time = 1.25
        self.last_message_timestamp = 1e10
        self.connected = False

    def connect(self):
        self.logger.log(f"Connecting to focuser at {self.address}...")
        try:
            self.device = Serial(self.address, baudrate=19200, timeout=0.25)
        except SerialException:
            return False
        # Set the focuser to the manual mode
        for i in range(5):
            self.device.write("FMMODE".encode())
            time.sleep(1)
            try:
                response = self.device.read(2).decode()
            except SerialException:
                return False
            if "!" in response:
                break
        else:
            self.logger.log("Can't set focuser to manual mode")
        self.logger.log(f"Connected to focuser at {self.address}")
        self.connected = True
        self.keep_connected = True
        threading.Thread(target=self.communicate).start()
        threading.Thread(target=self.watchdog).start()

    def disconnect(self):
        if not self.connected:
            return
        self.logger.log(f"Disconnecting from focuser at {self.address}...")
        self.device.close()
        self.connected = False
        self.device_info["Connected"] = False
        self.keep_connected = False
        # Wait for communication to shut down
        while 1:
            if self.communicating is False:
                break
            time.sleep(0.1)
        self.logger.log(f"Disconnected from focuser at {self.address}...")

    def reconnect(self):
        self.logger.log(f"Trying to reestablish connection to serial {self.address}")
        try:
            self.device.close()
        except SerialException:
            pass
        try:
            self.device = Serial(self.address, baudrate=19200, timeout=0.25)
        except SerialException:
            return False
        self.logger.log(f"Connection to focuser at {self.address} reestablished")

    def communicate(self):
        """
        Sends the messages from the out_queue to the device and
        recieves messages from the device and stores them in the
        in_queue. Runs in the background
        """
        self.communicating = True
        try:
            while self.connected:
                # Send the message
                try:
                    message = self.out_queue.get_nowait()
                except queue.Empty:
                    # No new messagen in the queue
                    message = None
                if message is not None:
                    try:
                        self.device.write(message.encode())
                    except SerialException:
                        # Connection is probably lost. But we do not handle it here. Let's watchdog
                        # detect the absence of the connection and does reconnection
                        pass
                # Read everything from the buffer
                try:
                    new_data = self.device.read(32).decode()
                    # print(new_data)
                except (SerialException, TypeError):
                    # Connection is probably lost. But we do not handle it here. Let's watchdog
                    # detect the absence of the connection and does reconnection
                    new_data = ""
                if new_data != "":
                    self.in_buffer += new_data
                    # print("buffer:", self.in_buffer)
                    self.parse_device_output()
                time.sleep(0.1)
        finally:
            self.communicating = False

    def move_inward(self, step):
        """
        Move focuser inward by a given step
        """
        if self.moving:
            return
        try:
            step = int(abs(step))
        except ValueError:
            return
        self.out_queue.put(f"FI{step:04}")
        self.moving = True

    def move_outward(self, step):
        """
        Move focuser inward by a given step
        """
        if self.moving:
            return
        try:
            step = int(abs(step))
        except ValueError:
            return
        print("qsize", self.out_queue.qsize())
        self.out_queue.put(f"FO{step:04}")
        self.moving = True

    def move_abs_position(self, new_position):
        """
        Move focuser to a new absolute position
        """
        try:
            new_position = int(abs(new_position))
        except ValueError:
            return
        step = new_position - self.position
        if step == 0:
            return
        if step < 0:
            # new position is lower: move inward
            self.move_inward(step)
            return
        else:
            self.move_outward(step)
            return

    def parse_device_output(self):
        while 1:
            # Parse data until there is something to parse
            if self.in_buffer == "":
                # Nothing to parse
                return
            lines = self.in_buffer.split("\n")
            if len(lines) == 0:
                # Nothing to parse
                return
            message = lines[0].strip()
            self.last_message_timestamp = time.time()  # Let's note when was the last message recieved
            self.in_buffer = "\n".join(lines[1:])
            if message == "!":
                # Focuser in the manual mode
                continue
            if message == "*":
                # Focuser says OK in responce to FI or FO command
                self.moving = False
                continue
            if message.startswith("P="):
                # Focuser returns it's position
                self.position = int(message.split("=")[1])
                continue
            if message.startswith("T="):
                # Focuser returns it's temerature
                self.temperature = float(message.split("=")[1])
                continue

    def device_info_updater(self):
        # The method has to obtain new values of the device parrameters and
        # also check if the connection is lost. Since the communication with
        # the device goes in an acyncroneous way, we cant rely on the get_position
        # and get_temperature functions returning NaNs. Instead we check how
        # much time have passed since the last message. If it is greater than
        # the timeout, the connection is probably lost
        # TODO: add list_ports.compots() here
        self.out_queue.put("FPOSRO")  # request new temerature value
        self.out_queue.put("FTMPRO")  # request new position value
        delta = time.time() - self.last_message_timestamp
        if self.moving:
            delta = 0
        if (delta > self.timeout) or (self.connected is False):
            self.device_info = {"Connected": False, "Position": None,
                                "Temperature": None, "Moving": None}
            return
        self.device_info = {"Connected": self.connected,
                            "Position": self.position,
                            "Temperature": self.temperature,
                            "Moving": self.moving}
