import threading
import time
from .base_device import BaseDevice
from .fli_lib import USBWheel
from .fli_lib import FLIError


class Wheel(BaseDevice):
    def __init__(self, out_queue, logger, serial_number, shift=0):
        super(Wheel, self).__init__(out_queue, logger)
        self.name = f"Wheel_{serial_number}"
        self.device = None
        self.device_info = {"Exists": False, "Connected": False, "Filter_pos": None,
                            "Moving": None, "Stepper_pos": None}
        self.selected_filter = 0
        self.shift = shift
        self.homed = False
        threading.Thread(target=self.locate_device, args=(serial_number, )).start()

    def __del__(self):
        self.try_locate = False

    def locate_device(self, serial_number):
        """ Locate the physical device by its serial number. This function has to be
        executed sucessfully before trying to esteblish the connection """
        self.try_locate = True
        error_reported = False
        while (self.try_locate is True) and (self.is_alive is True):
            try:
                self.device = USBWheel(serial_number)
                self.device_info["Exists"] = True
                if error_reported is True:
                    self.logger.log(f"Wheel {serial_number} was finally located")
                return
            except (FileNotFoundError, FLIError):
                # Device was not found
                if error_reported is False:
                    self.logger.log(f"Can't locate wheel {serial_number}... Will retry in the backround")
                    error_reported = True
                time.sleep(1)

    def connect(self):
        self.logger.log(f"Connecting to wheel {self.device.serial_number}...")
        if self.device.connect() is True:
            self.connected = True
            self.keep_connected = True
            threading.Thread(target=self.watchdog).start()
            self.logger.log(f"Connected to wheel {self.device.serial_number}")
            if not self.homed:
                self.logger.log(f"Homing wheel {self.device.serial_number} (shift {self.shift})")
                self.homed = self.device.home_device(self.shift)
            while 1:
                if self.device.get_filter_pos() >= 0:
                    print("filter ok")
                    break
                time.sleep(0.25)
                if self.device.is_moving():
                    print("waiting")
                    continue
                self.set_filter_pos(0)
                print("setting")
        else:
            self.logger.log(f"Failed to connect to wheel {self.device.serial_number}")

    def disconnect(self):
        if not self.connected:
            return
        self.logger.log(f"Disconnecting from wheel {self.device.serial_number}...")
        self.device.disconnect()
        self.connected = False
        self.keep_connected = False
        self.try_locate = False
        self.device_info["Connected"] = False
        self.device_info["Filter_pos"] = None
        self.device_info["Moving"] = None
        self.device_info["Stepper_pos"] = None
        self.logger.log(f"Disconnected from wheel {self.device.serial_number}")

    def reconnect(self):
        self.logger.log(f"Trying to reestablish connection to {self.device.serial_number}")
        if not self.connected:
            try:
                self.device.disconnect()
            except FLIError:
                # Not connected
                pass
        try:
            self.device.connect()
            # Not connected
        except FLIError:
            return
        if self.get_filter_pos() is None:
            # Not connected yet
            return
        # Finally connected
        self.connected = True
        self.device_info["Connected"] = True
        self.device_info_updater()
        # Home again
        self.logger.log(f"Homing wheel {self.device.serial_number} (shift {self.shift})")
        self.device.home_device(self.shift)
        # Wait for it to home
        while self.device.is_moving():
            time.sleep(0.5)
        # Restore the filter that was previously chosen
        for i in range(50):
            if self.set_filter_pos(self.selected_filter):
                break
            time.sleep(0.25)
        if not self.keep_connected:
            self.keep_connected = True
            threading.Thread(target=self.watchdog).start()

    def set_filter_pos(self, pos):
        if not self.device.is_moving():
            self.logger.log(f"Setting filter pos to {pos} for {self.device.serial_number} wheel")
            if self.device.set_filter_pos(pos):
                self.selected_filter = pos
                return True
        else:
            msg = f"Can't set new position to {self.device.serial_number} wheel: "
            msg += "already moving"
            self.logger.log(msg)
            return False

    def get_filter_pos(self):
        try:
            return self.device.get_filter_pos()
        except ValueError:
            return None

    def get_filter_count(self):
        return self.device.get_filter_count()

    def step_motor(self, step):
        self.device.step_motor(step)

    def device_info_updater(self):
        if self.device is None:
            return
        self.device_info["Connected"] = self.connected
        self.device_info["Filter_pos"] = self.get_filter_pos()
        self.device_info["Moving"] = self.device.is_moving()
        self.device_info["Stepper_pos"] = self.device.get_stepper_pos()
        if None in self.device_info.values():
            self.device_info["Connected"] = False
