import os
import time
from ctypes import c_char_p
from ctypes import c_long
from ctypes import c_double
from ctypes import byref
from ctypes import c_size_t
from ctypes import POINTER
from ctypes import CDLL
from ctypes import create_string_buffer
from threading import Lock

c_double_p = POINTER(c_double)
c_long_p = POINTER(c_long)

###############################################################################
# Library Definitions
###############################################################################
# An opaque handle used by library functions to refer to FLI hardware.

FLIDOMAIN_USB = 0x02
FLIDEVICE_FILTERWHEEL = 0x200
USB_WHEEL_DOMAIN = c_long(FLIDEVICE_FILTERWHEEL | FLIDOMAIN_USB)

# Status settings
FLI_FILTER_WHEEL_PHYSICAL = 0x100
FLI_FILTER_WHEEL_VIRTUAL = 0
FLI_FILTER_WHEEL_LEFT = FLI_FILTER_WHEEL_PHYSICAL | 0x00
FLI_FILTER_WHEEL_RIGHT = FLI_FILTER_WHEEL_PHYSICAL | 0x01
FLI_FILTER_STATUS_MOVING_CCW = 0x01
FLI_FILTER_STATUS_MOVING_CW = 0x02
FLI_FILTER_POSITION_UNKNOWN = 0xff
FLI_FILTER_POSITION_CURRENT = 0x200
FLI_FILTER_STATUS_HOMING = 0x00000004
FLI_FILTER_STATUS_HOME = 0x00000080
FLI_FILTER_STATUS_HOME_LEFT = 0x00000080
FLI_FILTER_STATUS_HOME_RIGHT = 0x00000040
FLI_FILTER_STATUS_HOME_SUCCEEDED = 0x00000008
ALL_MOVING_STATUSES = (FLI_FILTER_STATUS_MOVING_CCW, FLI_FILTER_STATUS_MOVING_CW,
                       FLI_FILTER_STATUS_HOMING)

_API_FUNCTION_PROTOTYPES = [
    # (c_long *dev, char *name, c_long domain);
    ("FLIOpen", [POINTER(c_long), c_char_p, c_long]),
    # (c_long dev);
    ("FLIClose", [c_long]),
    # (char* ver, size_t len);
    ("FLIGetLibVersion", [c_char_p, c_size_t]),
    # (c_long domain, char ***names);
    ("FLIList", [c_long, POINTER(POINTER(c_char_p))]),
    # (char **names);
    ("FLIFreeList", [POINTER(c_char_p)]),
    # (c_long dev, long filter);
    ("FLISetFilterPos", [c_long, c_long]),
    # (c_long dev, long *filter);
    ("FLIGetFilterPos", [c_long, c_long_p]),
    # (c_long dev, long *filter);
    ("FLIGetFilterCount", [c_long, c_long_p]),
    # (c_long dev, long steps);
    ("FLIStepMotor", [c_long, c_long]),
    # (c_long dev, long *position);
    ("FLIGetStepperPosition", [c_long, c_long_p]),
    # (c_long dev, long *steps);
    ("FLIGetStepsRemaining", [c_long, c_long_p]),
    # (c_long dev, long steps);
    ("FLIStepMotorAsync", [c_long, c_long]),
    # (c_long dev);
    ("FLIHomeDevice", [c_long]),
    # (c_long dev, char* serial, size_t len);
    ("FLIGetSerialString", [c_long, c_char_p, c_size_t]),
    # ("FLISetActiveWheel", [c_long, c_long]),
    # (c_long dev, long filter, char *name, size_t len);
    # ("FLIGetFilterName", [c_long, c_long, c_char_p, c_size_t])
    # (flidev_t dev, long *status);
    ("FLIGetDeviceStatus", [c_long, c_long_p]),
]


###############################################################################
# Error Handling
###############################################################################
class FLIError(Exception):
    pass


class FLIWarning(Warning):
    pass


def chk_err(err):
    """wraps a libfli C function call with error checking code"""
    if err < 0:
        msg = os.strerror(abs(err))  # err is always negative
        raise FLIError(msg)
    if err > 0:
        msg = os.strerror(err)      # FIXME, what if err is positive?
        raise FLIWarning(msg)
    return err


###############################################################################
# Library Loader
###############################################################################

# USB filter wheel device
class USBWheel(object):
    """ base class for all FLI USB devices"""
    def __init__(self, serial_number):
        """
        USB FLI filter wheel with a given serial number. The wheel object is
        disconnected by default. The constructor only finds it's device address.
        """
        self.device = None
        self.connected = False
        self.mutex = Lock()
        # load the DLL
        self.dll = CDLL("./libfli.so")
        for api_func_name, argtypes in _API_FUNCTION_PROTOTYPES:
            api_func = getattr(self.dll, api_func_name)
            api_func.argtypes = argtypes
            api_func.restype = chk_err

        self.serial_number = serial_number
        # Find all connected wheels
        all_wheels = POINTER(c_char_p)()
        self.dll.FLIList(USB_WHEEL_DOMAIN, byref(all_wheels))
        if all_wheels:
            # Check every wheel to find one with a given serial number
            idx = 0
            while all_wheels[idx]:
                dev_name, dev_model = all_wheels[idx].decode().split(";")
                # Open the device
                dev = c_long()
                self.dll.FLIOpen(byref(dev), dev_name.encode(), USB_WHEEL_DOMAIN)
                # Read it's serial number
                dev_serial = create_string_buffer(16)  # 16 bytes should be enough for a serial number
                self.dll.FLIGetSerialString(dev, dev_serial, c_size_t(16))
                self.dll.FLIClose(dev)
                if dev_serial.value.decode() == self.serial_number:
                    # We have found our device
                    self.device = dev
                    self.name = dev_name
                    break
                idx += 1
        # Close the list of wheels to free memory
        self.dll.FLIFreeList(all_wheels)
        if self.device is None:
            raise FileNotFoundError

    def set_filter_pos(self, pos):
        self.mutex.acquire()
        try:
            self.dll.FLISetFilterPos(self.device, c_long(pos))
            self.mutex.release()
            return True
        except FLIError:
            self.mutex.release()
            return False

    def get_filter_pos(self):
        pos = c_long()
        self.mutex.acquire()
        try:
            self.dll.FLIGetFilterPos(self.device, byref(pos))
            self.mutex.release()
        except FLIError:
            self.mutex.release()
            return None
        return pos.value

    def get_stepper_pos(self):
        pos = c_long()
        self.mutex.acquire()
        try:
            self.dll.FLIGetStepperPosition(self.device, byref(pos))
            self.mutex.release()
        except FLIError:
            self.mutex.release()
            return None
        return pos.value

    def get_filter_count(self):
        count = c_long()
        self.mutex.acquire()
        self.dll.FLIGetFilterCount(self.device, byref(count))
        self.mutex.release()
        return count.value

    def step_motor(self, step):
        step = c_long(int(step))
        self.mutex.acquire()
        self.dll.FLIStepMotor(self.device, step)
        self.mutex.release()

    def connect(self):
        if self.connected is True:
            return True
        try:
            self.dll.FLIOpen(self.device, self.name.encode(), USB_WHEEL_DOMAIN)
            self.connected = True
            return True
        except FLIError:
            return False

    def disconnect(self):
        self.connected = False
        self.dll.FLIClose(self.device)

    def is_moving(self):
        status = c_long()
        self.mutex.acquire()
        try:
            self.dll.FLIGetDeviceStatus(self.device, byref(status))
            self.mutex.release()
            return status.value in ALL_MOVING_STATUSES
        except FLIError:
            self.mutex.release()
            return None

    def home_device(self, shift):
        self.mutex.acquire()
        status = c_long()
        # Start homing process
        try:
            self.dll.FLIHomeDevice(self.device)
            print("Homing started")
        except FLIError:
            print("Error on homing")
            self.mutex.release()
            return False
        # Wait for homing to finish but not longer that for 5 seconds
        # (this should be more that enough)
        print("Waiting for homing to finish")
        time.sleep(4.75)
        print("Homing finished")
        if shift >= 0:
            print("Making shift")
            try:
                self.dll.FLIStepMotor(self.device, shift)
                time.sleep(0.5)
            except FLIError:
                print("Error on shifting")
                self.mutex.release()
                return False
        print("Shifting finished. Homing OK.")
        self.mutex.release()
        return True

    def __del__(self):
        if self.device is not None:
            try:
                self.dll.FLIClose(self.device)
            except FLIError:
                pass
