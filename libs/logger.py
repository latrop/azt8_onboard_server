from pathlib import Path
from datetime import datetime
import threading
import queue
import os


class Logger(object):
    def __init__(self, debug):
        self.debug = debug
        log_path = Path(__file__).parent.parent / "logs"
        if not log_path.exists():
            os.makedirs(log_path)
        self.log_queue = queue.Queue()
        # Keep at max 10 log files in the directory
        log_files = sorted(log_path.glob("*.log"),
                           key=lambda x: x.stat().st_mtime)
        for fname in log_files[:-10]:
            os.remove(fname)
        # Create new log file
        now = datetime.now()
        log_name = f"{now.year}.{now.month:02}.{now.day:02}_"
        log_name += f"{now.hour:02}:{now.minute:02}:{now.second:02}.log"
        self.log_file = open(log_path / log_name, "w")
        threading.Thread(target=self.logging).start()
        self.log("Log started")

    def log(self, line):
        self.log_queue.put(line)

    def logging(self):
        while 1:
            line = self.log_queue.get()
            if line is None:
                break
            now = datetime.now()
            t_stamp = f"{now.year}.{now.month:02}.{now.day:02} "
            t_stamp += f"{now.hour:02}:{now.minute:02}:{now.second:02}"
            self.log_file.write(f"{t_stamp}  {line}\n")
            if self.debug is True:
                self.log_file.flush()
