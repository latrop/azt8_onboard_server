import socket
import threading
import queue
import time
import json
from .devices import focuser
from .devices import rotator
from .devices import wheel
from . import logger


class Server(object):
    def __init__(self, port, debug=True):
        self.working = True
        # Create a log file
        self.logger = logger.Logger(debug)
        # Buffer for a recieved data
        self.recv_buffer = ""
        # A queue for a data to send
        self.comm_queue = queue.Queue(maxsize=128)
        # Create a socket
        self.server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.server_socket.bind(("", port))
        # And start listening for a connection
        self.server_socket.listen(0)  # Do not allow unaccepted connections hanging around
        self.client_connected = False

        # Create devices objects. If debug option is true, we create
        # fake devices to simulate the communication with them
        self.devices = {}
        # if debug is True:
        #     # self.devices["focuser"] = focuser.FakeFocuser(self.comm_queue, self.logger)
        #     # self.devices["rotator"] = rotator.FakeRotator(self.comm_queue, self.logger)
        #     # self.devices["wheel_1"] = wheel.FakeWheel(self.comm_queue, self.logger)
        #     # self.devices["wheel_2"] = wheel.FakeWheel(self.comm_queue, self.logger)
        # else:
        self.devices["focuser"] = focuser.Focuser(self.comm_queue, self.logger, "/dev/ttyUSB0")
        self.devices["rotator"] = rotator.Rotator(self.comm_queue, self.logger, "/dev/ttyUSB1")
        #     self.devices["wheel_1"] = wheel.Wheel(self.comm_queue, self.logger, 1)
        #     self.devices["wheel_2"] = wheel.Wheel(self.comm_queue, self.logger, 2)
        self.devices["wheel_1"] = wheel.Wheel(self.comm_queue, self.logger, "CF10051918", shift=1)
        self.devices["wheel_2"] = wheel.Wheel(self.comm_queue, self.logger, "CF10564218", shift=1)

    def wait_for_connection(self):
        """
        Listens for a connection from a client
        """
        self.client, address = self.server_socket.accept()
        self.client.settimeout(0.1)
        self.client_connected = True
        with self.comm_queue.mutex:
            # Remove all messages from the previous session
            self.comm_queue.queue.clear()
        self.logger.log_queue.put(f"Got connection from {address}")
        threading.Thread(target=self.communicate).start()
        threading.Thread(target=self.device_reporter).start()

    def communicate(self):
        while self.working is True:
            # Try to read any incomming data
            try:
                new_data = self.client.recv(1024)
                if not new_data:
                    break
                new_data = new_data.decode()
                self.recv_buffer += new_data
                # If there is new data, we try to extract all commands from it
                while 1:
                    message = self.process_buffer()
                    if message is not None:
                        # We have got a new message from a client. Let's send it to the
                        # destination device
                        self.send_command(message)
                    else:
                        break
            except socket.timeout:
                # No data got from the buffer
                pass
            except OSError:
                # Some problem with the connection. Lets close it and wait for a new one
                break
            # See if there is any data from the devices in 'to send' queue
            try:
                problem_occured = False
                while self.comm_queue.qsize():
                    message_to_send = self.comm_queue.get(block=False)
                    try:
                        self.client.send(message_to_send.encode())
                    except OSError:
                        # Some problem with the connection. Lets close it and wait for a new one
                        problem_occured = True
                        break
                if problem_occured:
                    break
            except queue.Empty:
                # No data to sent.
                pass
            # Wait a bit
            time.sleep(0.1)
        self.logger.log("Client disconnected")
        self.client.close()
        self.client_connected = False

    def process_buffer(self):
        """
        The commands are separated by a new line sign. But it can happen that we will
        read a part of a command from the input connection, so we need to check the
        buffer and extract only the whole commands by splitting it by '\n'
        """
        if "\n" not in self.recv_buffer:
            # No whole command in the buffer.
            return None
        # If there are '\n' symbols in the buffer we take out the first line
        lines = self.recv_buffer.split("\n")
        message = lines[0]
        self.recv_buffer = "\n".join(lines[1:])
        return message

    def send_command(self, message):
        """
        This function takes a message, parses it and translates it to the destination device
        A message should be a JSON containing a dictionary with next keys:
        device -- a device to send the command to
        command -- a command to run on the device
        parameters -- a list of parameters to provide to the command
        """
        # Unpack the message
        try:
            message_content = json.loads(message)
        except json.JSONDecodeError:
            self.logger.log(f"[Error] Wrong command: {message}")
        print("<<<" + message + ">>>")
        dev_name = message_content["device"]
        command_name = message_content["command"]
        params = message_content["parameters"]
        if dev_name in self.devices:
            # Retranslate the command to the device if there is such device
            self.logger.log(f"Sending command '{command_name}' to '{dev_name}'")
            threading.Thread(target=self.devices[dev_name].run_command, args=(command_name, params)).start()
        elif dev_name == 'server':
            # The command is intended to be run by the server itself
            try:
                command = getattr(self, command_name)
            except AttributeError:
                # No such command in the server
                self.logger.log(f"[Error] Server does not have '{command_name}' command")
            command(*params)
        elif dev_name == 'all_devices':
            # The command is for all devices
            for dev_name, dev in self.devices.items():
                self.logger.log(f"Sending command '{command_name}' to '{dev_name}'")
                threading.Thread(target=dev.run_command, args=(command_name, params)).start()
        else:
            self.logger.log(f"[Error] No such device: {dev_name}")

    def device_reporter(self):
        """
        Method runs in a background, gathers state of all devices and sends it to
        the client
        """
        print("dev reporter started")
        while (self.working is True) and (self.client_connected is True):
            time.sleep(0.2)
            all_data = {}
            for dev_name, dev in self.devices.items():
                all_data[dev_name] = dev.get_state()
            print(all_data)
            self.comm_queue.put(json.dumps(all_data))
        print("dev reporter ended")

    def disconnect_all_devices(self):
        for dev_name, dev in self.devices.items():
            self.logger.log(f"Shutting down {dev_name}")
            dev.delete()

    def shutdown(self):
        self.working = False
        self.server_socket.close()
        self.disconnect_all_devices()
        self.logger.log("Server shut down")
        self.logger.log(None)
