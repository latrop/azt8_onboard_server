#! /usr/bin/env python

import time
import threading
from libs import server

s = server.Server(6546)

def running_guard(server_object):
    while server_object.working is True:
        if not server_object.client_connected:
            print("Main: wait for connection")
            server_object.wait_for_connection()
            print("after waiting")
        else:
            print("Connected")
        time.sleep(1)

# Running the server in the background allows for the main process
# not to crush with it and try to close all things as properly as
# possible in the end
t = threading.Thread(target=running_guard, args=(s,))
t.start()

while t.is_alive():
    time.sleep(1)
try:
    s.shutdown()
finally:
    exit(1)