#! /usr/bin/env python

import socket
import threading
import json
import time


class Client(object):
    def __init__(self):
        self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.s.connect(("192.168.1.16", 6543))
        self.s.settimeout(0.5)
        self.is_running = True
        threading.Thread(target=self.listen).start()

    def listen(self):
        while self.is_running:
            try:
                data = self.s.recv(1024).decode()
                if data:
                    print(data)
            except socket.timeout:
                pass
            time.sleep(0.1)
        self.s.close()

    def close(self):
        self.is_running = False

    def send_command(self, device, command, parameters):
        data = {"device": device, "command": command, "parameters": parameters,
                "send_back": True}
        msg = json.dumps(data) + "\n"
        self.s.send(msg.encode())
